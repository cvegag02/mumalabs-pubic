# mumalabs

Note: You need to have vuejs, express, nodejs and vuetify

## Steps to run the application locally

The steps detailed below will have to be performed in the order that they are explained.

### Create a database in MYSQL Workbench

Create and run a database called mumalabs

### Modifications to the code

- Access /mumalabs/backend/config/db.config.js and modify Host by the host of your database. Change the user for which you have in your database. Modify the password for that of your database.

- Access /mumalabs/frontend/mumalabs/src/config/IPbackend.js and modify const ipBackend by your backend IP.

### Commans on the backend

- Access /mumalabs/backend and run the following commands:
    $ npm install
    $ nodemon start

### Commans on the frontend

- Access /mumalabs/frontend/mumalabs and run de following commands:
    $ yarn install
    $ yarn serve