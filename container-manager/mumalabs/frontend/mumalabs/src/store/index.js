import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex);

const store = new Vuex.Store({
  plugins: [createPersistedState()],

  state: {
    user: null,
    token: null,
    userContainers: null,
    containers: null,
    images:null
  },
  mutations: {
    SET_USER_DATA(state, data) {
      state.user = data.usuario;
      state.token = data.tokenReturn;
      localStorage.setItem("user", JSON.stringify(data.usuario));
      localStorage.setItem("token", data.tokenReturn);
    },

    CLEAR_USER_DATA(state) {
      localStorage.removeItem("user");
      localStorage.removeItem("token");
      state.user = null;
      state.token = null;
      location.reload();
    },

    SET_TOKEN(state, token) {
      state.token = token;
      localStorage.setItem("token", token);
    },

    SET_CONTAINER_DATA(state, data) {
      state.containers = data;
    },

    SET_CONTAINERS_DATA(state, data) {
      state.containers = data;
      
    },

    SET_IMAGE_DATA(state, data) {
      state.images = data;
    },

    SET_IMAGES_DATA(state, data) {
      state.images = data;
      
    },

    SET_USERCONTAINERS_DATA(state, data) {
      state.userContainers = data;
    }
    
  },
  actions: {

    login({ commit }, data) {
      commit("SET_USER_DATA", data);
    },

    setToken({ commit }, data) {
      commit("SET_TOKEN", data);
    },

    containers({ commit }, data) {
      commit("SET_CONTAINERS_DATA", data);
    },

    images({ commit }, data) {
      commit("SET_IMAGES_DATA", data);
    },

    userContainers({ commit }, data) {
      commit("SET_USERCONTAINERS_DATA", data);
    },

    logout({ commit }) {
      commit("CLEAR_USER_DATA");
    }
  },
  getters: {
    loggedIn(state) {
      return !!state.user;
    },
    getToken(state) {
      return state.token;
    }
  }
});
export default store;
