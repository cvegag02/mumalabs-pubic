import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import BaseIcon from "@/components/BaseIcon";
import upperFirst from "lodash/upperFirst";
import camelCase from "lodash/camelCase";
import moment from "moment";
import imagesService from "./services/ImagesService.js";

//PARA HACER BaseIcon componente global
Vue.component("BaseIcon", BaseIcon);

Vue.config.productionTip = false;

const requireComponent = require.context(
  // todos los archivos afín a la expresion regular son globales
  "./components",
  false,
  /Base[A-Z]\w+\.(vue|js)$/
);

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName);

  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\/(.*)\.\w+$/, "$1"))
  );

  Vue.component(componentName, componentConfig.default || componentConfig);
});
//------------------------

Vue.config.productionTip = false;

//Formato de la fecha
Vue.filter("formatDate", function(value) {
  if (value) {
    return moment(String(value)).format("MM/DD/YYYY hh:mm");
  }
});

new Vue({
  router,
  store,
  vuetify,
  created() {
    const userToken = localStorage.getItem("token");
    console.log("-------------------" + userToken + "\n---------------");
  },
  render: h => h(App)
}).$mount("#app");

setInterval(function() {
  imagesService.getImagesList().then(response => {
    this.$store.dispatch("images", response.data);
  });
}, 30 * 60 * 1000);
