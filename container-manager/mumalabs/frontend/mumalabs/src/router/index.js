import Vue from "vue";
import VueRouter from "vue-router";
import loginRegisterSevice from "../services/LoginRegisterService.js";
import store from "../store/index";
Vue.use(VueRouter);

const routes = [
  {
    path: "/app/user/:username",
    name: "app",
    props: true,
    meta: { requiresAuth: true },
    component: () => import("../views/AppLayout.vue"),
    children: [
      {
        path: "home",
        component: () => import("../views/Home.vue"),
        meta: { requiresAuth: true }
      },
      {
        path: "images",
        component: () => import("../views/Image.vue"),
        meta: { requiresAuth: true }
      },
      {
        path: "mis-userContainers",
        component: () => import("../views/Mis-userContainers.vue"),
        meta: { requiresAuth: true }
      },
      {
        path: "mi-perfil",
        component: () => import("../views/Mi-perfil.vue"),
        meta: { requiresAuth: true }
      }
    ]
  },

  {
    path: "/login",
    name: "Login",
    alias: "/",
    component: () => import("../views/Login.vue")
  },

  {
    path: "/registro",
    name: "Registro",
    component: () => import("../views/Registro.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

//CHECKEAMOS QUE TENGA TOKEN DE ACUERDO CON LA Página
router.beforeEach((to, from, next) => {
  let token = localStorage.token;
  let autorizacion = to.matched.some(record => record.meta.requiresAuth);

  if (autorizacion) {
    loginRegisterSevice
      .postCheck({
        name: to.params.username,
        secretToken: token
      })

      .then(function(response) {
        console.log(response);
        store.dispatch("setToken", response.data.token);
        next();
      })

      .catch(function() {
        next("/login");
      });
  } else if (!autorizacion) {
    next();
  }
});

export default router;
