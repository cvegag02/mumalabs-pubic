import axios from "axios";
import IP from "../config/IPbackend";

const apiImages = axios.create({
  baseURL: `http://${IP.IP_BACKEND}:3000`,
  withCredentials: false, // This is the default
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    token: localStorage.token
  }
});

export default {
  getImagesList() {
    return apiImages.get("/api/images/");
  },

  postUserContainer(image, usuarioId) {
    return apiImages.post("/api/userContainers/user/" + usuarioId, image);
  },

  /* REVISAR EL postContainer() */
  postContainer(image) {
    return apiImages.post("/api/containers/", image);
  },

  postImage(image) {
    return apiImages.post("api/images/", image);
  },

  getImages(imageId) {
    return apiImages.get("/api/images/" + imageId).catch(() => {});
  }
};
