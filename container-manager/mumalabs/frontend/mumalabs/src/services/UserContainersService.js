import axios from "axios";
import IP from "../config/IPbackend";

const apiUserContainers = axios.create({
  baseURL: `http://${IP.IP_BACKEND}:3000`,
  withCredentials: false, // This is the default
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    token: localStorage.token
  }
});

export default { 
  getUserContainersList() {
    return apiUserContainers.get("/api/userContainers/");
  },
  
  deleteUserContainer(container, userContainerId) {
    return apiUserContainers.delete(
      "/api/userContainers/" + userContainerId,
      container
    );
  },

  modifiedUserContainer(nuevaUserContainer, userContainerId) {
    return apiUserContainers.put(
      "/api/userContainers/" + userContainerId,
      nuevaUserContainer
    );
  },

  getUserContainersByUser(userId) {
    return apiUserContainers
      .get("/api/userContainers/user/" + userId)
      .catch(() => {});
  }
};
