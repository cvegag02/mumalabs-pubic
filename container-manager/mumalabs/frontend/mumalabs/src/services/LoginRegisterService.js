import axios from "axios";
import IP from "../config/IPbackend.js";

const apiClient = axios.create({
  baseURL: `http://${IP.IP_BACKEND}:3000`,
  withCredentials: false, // This is the default
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
});

export default {
  postRegister(user) {
    return apiClient.post("/api/usuarios", user);
  },
  postLogin(credential) {
    return apiClient.post("/api/login", credential);
  },
  getClientList() {
    return apiClient.get("/api/usuarios/");
  },
  postCheck(data) {
    return apiClient.post("/api/login/checktoken", data);
  }
};
