import axios from "axios";
import IP from "../config/IPbackend";

const apiContainers = axios.create({
  baseURL: `http://${IP.IP_BACKEND}:3000`,
  withCredentials: false, // This is the default
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    token: localStorage.token
  }
});

export default {
  getContainersList() {
    return apiContainers.get("/api/containers/");
  },

  postUserContainer(container, usuarioId) {
    return apiContainers.post(
      "/api/userContainers/user/" + usuarioId,
      container
    );
  },

  getContainers(containerId) {
    return apiContainers.get("/api/containers/" + containerId).catch(() => {});
  },

  deleteContainer(containerId) {
    return apiContainers.delete("/api/containers/" + containerId);
  },

  modifiedContainer(nuevaContainer, containerId) {
    return apiContainers.put("/api/containers/" + containerId, nuevaContainer);
  }
};
