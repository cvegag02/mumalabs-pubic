module.exports = (sequelize, Sequelize) => {
  const Image = sequelize.define("images", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },
    name: {
      type: Sequelize.STRING, 
    }
  });
  return Image;
};
  