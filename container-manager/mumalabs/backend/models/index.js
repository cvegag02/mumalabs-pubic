const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.usuarios = require("./Usuario.model.js")(sequelize, Sequelize);
db.userContainers = require("./UserContainer.model.js")(sequelize, Sequelize);
db.containers = require("./Container.model.js")(sequelize, Sequelize);
db.images = require("./Image.model.js")(sequelize, Sequelize);

//RELACION ONE TO MANY USUARIOS a CONTENEDORES EN USO
db.usuarios.hasMany(db.userContainers, { as: "userContainers" });
db.userContainers.belongsTo(db.usuarios, {
  foreignKey: "usuarioId",
  as: "usuarios"
});


//RELACION ONE TO MANY CONTENEDORES a CONTENEDORES EN USO 
db.containers.hasMany(db.userContainers, { as: "userContainers" });
db.userContainers.belongsTo(db.containers, {
  foreignKey: "containerId",
  as: "containers"
});


//RELACION ONE TO MANY IMAGES a CONTENEDORES
db.images.hasMany(db.containers, { as: "containers" });
db.containers.belongsTo(db.images, {
  foreignKey: "imageId",
  as: "images"
});

module.exports = db;
