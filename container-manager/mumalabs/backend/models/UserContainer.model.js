module.exports = (sequelize, Sequelize) => {
  const UserContainer = sequelize.define("userContainers", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    }
  });
  return UserContainer;
};
 