module.exports = {
  HOST: "127.0.0.1", /* Change host to the database host */
  USER: "root", /* Change user for the database */
  PASSWORD: "15081998", /* Change password for the database */
  DB: "mumalabs", /* Change the name to the database */
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
};
