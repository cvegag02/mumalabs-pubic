module.exports = (app) => {
  const userContainers = require("../../controllers/userContainer.controller");
  var router = require("express").Router();
  const auth = require("../../middleware/auth");

  router.get("/user/:userid", userContainers.getUserContainersByUser);
  router.post("/user/:userid", userContainers.setUserContainerByUser);
  
  // Retrieve all containers
  router.get("/", userContainers.findAll);

  // Retrieve a single containers with id
  router.get("/:id", userContainers.findOne);

  // Update a UserContainer with id
  router.put("/:id", userContainers.update);

  // Delete a UserContainer with id
  router.delete("/:id", userContainers.delete);

  app.use("/api/userContainers", router);
};
