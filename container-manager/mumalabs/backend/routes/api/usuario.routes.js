module.exports = (app) => {
  const usuarios = require("../../controllers/usuario.controller");
  var router = require("express").Router();
  const auth = require("../../middleware/auth");

  // Create a new Usuarios
  router.post("/", usuarios.create);

  // Retrieve all Usuarios
  router.get("/", usuarios.findAll);

  // Retrieve a single Usuarios with id
  router.get("/:id", usuarios.findOne);

  // Update a Usuarios with id
  router.put("/:id",usuarios.update);

  // Delete a Usuarios with id
  router.delete("/:id", usuarios.delete);

  // Create a new Usuarios
  router.delete("/", usuarios.deleteAll);

  app.use("/api/usuarios", router);
};
