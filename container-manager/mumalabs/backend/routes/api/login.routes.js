module.exports = (app) => {
  const login = require("../../controllers/login.controller");
  var router = require("express").Router();
  
  // Create a new Usuarios
  router.post("/", login.login);
  router.post("/checktoken",login.checkToken);
  app.use("/api/login", router);
    
};