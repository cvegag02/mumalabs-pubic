module.exports = (app) => {
  const images = require("../../controllers/image.controller");
  var router = require("express").Router();
  
  // Create a new images
  router.post("/", images.create);
  
  // Retrieve all images
  router.get("/", images.findAll);
  
  // Retrieve a single images with id
  router.get("/:id", images.findOne);
  
  // Update a images with id
  router.put("/:id", images.update);
  
  // Delete a images with id
  router.delete("/:id", images.delete);
  
  // Delete all images
  router.delete("/", images.deleteAll);
    
  app.use("/api/images", router);
};
  