module.exports = (app) => {
  const containers = require("../../controllers/container.controller");
  var router = require("express").Router();

  // Create a new containers
  router.post("/", containers.create);
  
  // Retrieve all containers
  router.get("/", containers.findAll);
  
  // Retrieve a single containers with id
  router.get("/:id", containers.findOne);
  
  // Update a containers with id
  router.put("/:id", containers.update);
  
  // Delete a containers with id
  router.delete("/:id", containers.delete);
  
  // Delete all containers
  router.delete("/", containers.deleteAll);
    
  app.use("/api/containers", router);
}; 
  