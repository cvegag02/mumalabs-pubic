const db = require("../models");
const Usuario = db.usuarios;
const express = require("express");
const { check, validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const Op = db.Sequelize.Op;
const token = require("../services/token")

exports.login = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  const { name, password } = req.body;
  try {
    let usuario = await Usuario.findOne({
      where: { name: name },
    });
    
    // Check if account exists
    if (!usuario) {
      return res.status(400).json({
        errors: [
          {
            msg: "The account does not exist",
          },
        ],
      });
    }

    // Check password
    const isMatch = await bcrypt.compare(password, usuario.password);
    if (!isMatch) {
      return res.status(400).json({
        errors: [
          {
            msg: "Credentiales Invalidas",
          },
        ],
      });
    }
      
    delete usuario["dataValues"]["password"];
    let tokenReturn = await token.encode(usuario.id,usuario.name);
    res.json({usuario,tokenReturn});
      
  } catch (e) {
    console.error(e.message);
    res.status(500).send("Server error");
  }
} 

exports.checkToken= (req, res)=> {
  try{
    const { name, secretToken } = req.body;
 
    if(!name||!secretToken)
      res.status(400).send("Server error NO TOKEN");
  
    token.decode(secretToken,name).then(
      (newtoken)=>{
        if(newtoken){
          res.json({"token":newtoken,"correcto":true,});
        }else{
          res.status(400).send(" WRONG TOKEN ");
        }
      }
    ).catch(()=>{
      console.log("Wrong token")
    })
  }catch (e) {
    res.status(500).send("Server Error");
  }
}
