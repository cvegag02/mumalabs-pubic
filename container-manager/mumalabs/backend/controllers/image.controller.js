const db = require("../models");
const Usuario = db.usuarios;
const UserContainer = db.userContainers;
const Container = db.containers;
const Image = db.images;

const bcrypt = require("bcryptjs");

const Op = db.Sequelize.Op;

// Create and Save a new Image
exports.create = async (req, res) => {
  // Validate request
  
  if (!req.body.name ) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }
  
  try {
    const cont = await Image.findOne({
      where: { name: req.body.name },
    });
  
    if (cont) {
      return res
        .status(404)
        .json({ msg: "Error 404! -> A Image with the same name already exists" });
    }
  

    // Create a Image
    const image = {
      name: req.body.name,
    };
  
    // Save Image in the database
    await Image.create(image).then((image) => {
      res.json({
        image,
      });
    });
  } catch (exception) {
    console.error(exception.message);
    res.status(500).send("Error!");
  }
};
  

  

//----------------------NOT Tested ----------------------------------------
  
// Retrieve all Images by name from the database.
exports.findAll = (req, res) => {
  const name = req.query.name;
  var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;
  
  Image.findAll({ where: condition })
  .then((data) => {
    res.send(data);
  })
  .catch((err) => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving tutorials.",
      });
  });
};
  
// Find a single Usuario with a name
exports.findOne = (req, res) => {
  const id = req.params.id;
  
  Image.findByPk(id)
  .then((data) => {
    res.send(data);
  })
  .catch((err) => {
    res.status(500).send({
      message: "Error retrieving image with id=" + id,
    });
  });
};
  
// Update an Image by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;
  
  Image.update(req.body, {
    where: { id: id },
  })
  .then((num) => {
    if (num == 1) {
      res.send({
        message: "Image was updated successfully.",
      });
    } else {
      res.send({
        message: `Cannot update Image with id=${id}. Maybe Image was not found or req.body is empty!`,
      });
    }
  })
  .catch((err) => {
    res.status(500).send({
      message: "Error updating Image with id=" + id,
    });
  });
};
  
// Delete a Tutorial with the specified name in the request
exports.delete = (req, res) => {
  const name = req.params.name;
  
  Usuario.destroy({
    where: { name: name },
  })
  .then((num) => {
    if (num == 1) {
      res.send({
        message: "User was deleted successfully!",
      });
    } else {
      res.send({
        message: `Cannot delete Tutorial with id=${id}. Maybe Tutorial was not found!`,
      });
    }
  })
  .catch((err) => {
    res.status(500).send({
      message: "Could not delete Tutorial with id=" + id,
    });
  });
};
  
// Delete all Usuarios from the database.
exports.deleteAll = (req, res) => {
  Usuario.destroy({
    where: {},
    truncate: false,
  })
  .then((nums) => {
    res.send({ message: `${nums} Tutorials were deleted successfully!` });
  })
  .catch((err) => {
    res.status(500).send({
      message:
      err.message || "Some error occurred while removing all tutorials.",
    });
  });
};
  